import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class Hanoi implements ActionListener, AdjustmentListener{

	JButton resoudre;
	boolean resolution;
	public static JLabel nbreEssais;
	public Affichage aff;
	private JScrollBar tempsActualisation;
	public void HonoiExecution(){
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int nbreDisques = 0;
		boolean tmp;
		while (nbreDisques == 0){
			String chaine = JOptionPane.showInputDialog(null,null,"Nombre de disques",JOptionPane.QUESTION_MESSAGE);
			if (chaine == null){
				System.exit(0);
			}
			tmp = true;
			try{
				nbreDisques = Integer.parseInt(chaine);
			}
			catch (Exception e){	
				tmp = false;
				JOptionPane.showMessageDialog(null ,"Saisie incorrecte","attention",JOptionPane.ERROR_MESSAGE);
			}
			if (((nbreDisques>20) || (nbreDisques < 2)) && tmp){
				JOptionPane.showMessageDialog(null ,"nombre de disque trop grand ou trop faible !","attention",JOptionPane.ERROR_MESSAGE);			
				nbreDisques = 0;
			}

		}
		JFrame f = new JFrame("tours de Hano� - par Christopher GRAND");
		f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		f.addWindowListener(new EcouteurFenetre());
		f.setSize(new Dimension(Affichage.LARG,Affichage.HAUT+90));


		JPanel panneau = new JPanel();
		JPanel infos = new JPanel();
		infos.setLayout(new GridLayout(1,3));
		resoudre = new JButton("resolution automatique : non");
		resoudre.addActionListener(this);
		resolution = false;
		infos.add(resoudre);
		nbreEssais = new JLabel("nombre d'essais : "+0);
		infos.add(nbreEssais);
		JPanel decompte = new JPanel();
		decompte.setLayout(new GridLayout(2,1));
		decompte.add(new JLabel("temps d'actualisation(en ms)"));
		this.tempsActualisation = new JScrollBar();
		this.tempsActualisation.setOrientation(JScrollBar.HORIZONTAL);
		this.tempsActualisation.setMinimum(1);
		this.tempsActualisation.setMaximum(110);
		this.tempsActualisation.setValue(50);
		this.tempsActualisation.setPreferredSize(new Dimension(200,20));
		
		tempsActualisation.addAdjustmentListener(this);
		decompte.add(tempsActualisation);
		infos.add(decompte);
		panneau.add(infos);
		aff = new Affichage(nbreDisques,this);
		panneau.add(aff);
		f.add(panneau);
		f.setVisible(true);
		

	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == this.resoudre){
			if (this.resolution){
				this.resoudre.setText("resolution automatique : non");
				this.resolution = false;
				aff.setResolution(false);
			}
			else{
				this.resoudre.setText("resolution automatique : oui");
				this.resolution = true;
				aff.setResolution(true);
			}
		}
		
	}

	public long getTemps() {//si on modifie le temps dans le champ alors on met a jour
		return (-this.tempsActualisation.getValue()*2000+200000)/99;
	}

	
	public void adjustmentValueChanged(AdjustmentEvent ae) {
		if (ae.getSource() == this.tempsActualisation){

			((Etapes) this.aff.th).setTemps(getTemps());
		}
		
	}

}
