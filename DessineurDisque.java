import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;


public class DessineurDisque {
	Graphics2D g;
	Color c;
	public DessineurDisque(Graphics g) {
		this.g = (Graphics2D) g;
		this.c = Color.BLACK;
	}
	public void setCouleur(Color c){
		this.c = c;
	}
	public void dessinerOvale(int x,int y,int width, int height){
		GradientPaint gp = new GradientPaint(x, y, c, x+width/4, y+3*height/4, new Color((c.getRed()+255)/2,(c.getGreen()+255)/2,(c.getBlue()+255)/2), true);
		g.setPaint(gp);
		//g.setColor(c);
		g.fillOval(x, y, width, height);
	}

}
