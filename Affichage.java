import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;


import javax.swing.*;

public class Affichage extends JPanel implements MouseMotionListener,MouseListener{
	
	private static final long serialVersionUID = 1L;
	int nbreDisques;
	static final int LARG = 800;
	static final int HAUT = 450;
	public static final int LARGEURSUPPORT = 4;
	public static final int XUN = LARG/6;
	public static final int XDEUX = XUN+LARG/3;
	public static final int XTROIS = XDEUX+LARG/3;
	public static final int ECARTDEUXDISQUES = 15;
	public static final int BASE = 20;
	public static final int TAILLEXUN = 6;
	public static final int TAILLEYUN = 20;
	public static final int SUPPLEMENTHAUTEURSUPPORT = 20;
	Vector<Integer>[] piles;

	int hauteurSupport;
	int YSupport;
	private DessineurDisque dd;
	private Color[] couleurs;
	private int disqueEnDeplacement;
	private int xDeplacement;
	private int yDeplacement;
	private int origineDisque;
	int nbreEtapes;
	private boolean resolution;
	Thread th;
	Hanoi h;
	private boolean affichagePasFini;
	
	@SuppressWarnings("unchecked")
	public Affichage(int nbreDisques,Hanoi h) {

		this.h = h;
		this.nbreEtapes = 0;
		this.resolution = false;
		piles = new Vector[3]; 
		piles[0] = new Vector<Integer>();
		piles[1] = new Vector<Integer>();
		piles[2] = new Vector<Integer>(); 

		this.setPreferredSize(new Dimension(LARG,HAUT));
		this.nbreDisques = nbreDisques;
		for (int i = nbreDisques;i > 0;i-- ){
			piles[0].add(i);
		}
		couleurs = new Color[3];
		couleurs[0] = Color.BLUE;
		couleurs[1] = Color.RED;
		couleurs[2] = Color.GREEN;

		this.hauteurSupport = nbreDisques * ECARTDEUXDISQUES+SUPPLEMENTHAUTEURSUPPORT;
		this.YSupport = HAUT-this.hauteurSupport-BASE;
		this.dd = null;
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		this.disqueEnDeplacement = 0;
		th = new Etapes(piles,this);
		this.repaint();
	}

	public boolean isResolution() {
		return resolution;
	}

	@SuppressWarnings("deprecation")
	public void setResolution(boolean resolution) {
		this.resolution = resolution;
		this.reset();
		this.repaint();
		if (resolution){
			this.th.start();
		}
		else{
			
			this.th.stop();
			this.th = new Etapes(piles,this);
		}
		this.repaint();
	}
	public void reset(){
		this.piles[0].removeAllElements();
		this.piles[1].removeAllElements();
		this.piles[2].removeAllElements();			
		for (int i = nbreDisques;i > 0;i-- ){
			piles[0].add(i);
		}			
		this.nbreEtapes = 0;
		Hanoi.nbreEssais.setText("nombre d'essais : "+this.nbreEtapes);
		this.hauteurSupport = this.nbreDisques * ECARTDEUXDISQUES+SUPPLEMENTHAUTEURSUPPORT;
		this.YSupport = HAUT-this.hauteurSupport-BASE;
	}

	@Override
	protected void paintComponent(Graphics g) {
		this.affichagePasFini = true;
		super.paintComponent(g);

		dd = new DessineurDisque(g);

		g.setColor(Color.BLACK);

		g.drawRect(XUN,YSupport , LARGEURSUPPORT, hauteurSupport);
		g.drawRect(XDEUX,YSupport, LARGEURSUPPORT, hauteurSupport);
		g.drawRect(XTROIS,YSupport, LARGEURSUPPORT, hauteurSupport);
		int disque;

		for (int i=0;i<piles[0].size();i++){
			disque = piles[0].elementAt(i);
			dd.setCouleur(couleur(disque));
			int x,y,width, height;
			x = XUN - disque*TAILLEXUN+LARGEURSUPPORT/2;
			y = HAUT-BASE-TAILLEYUN-i*ECARTDEUXDISQUES;
			width = 2*disque*TAILLEXUN;
			height = TAILLEYUN;
			dd.dessinerOvale(x, y, width, height);

		}
		for (int i=0;i<piles[1].size();i++){
			disque = piles[1].elementAt(i);
			dd.setCouleur(couleur(disque));
			int x,y,width, height;
			x = XDEUX - disque*TAILLEXUN+LARGEURSUPPORT/2;
			y = HAUT-BASE-TAILLEYUN-i*ECARTDEUXDISQUES;
			width = 2*disque*TAILLEXUN;
			height = TAILLEYUN;
			dd.dessinerOvale(x, y, width, height);
		}
		for (int i=0;i<piles[2].size();i++){
			disque = piles[2].elementAt(i);
			dd.setCouleur(couleur(disque));
			int x,y,width, height;
			x = XTROIS - disque*TAILLEXUN+LARGEURSUPPORT/2;
			y = HAUT-BASE-TAILLEYUN-i*ECARTDEUXDISQUES;
			width = 2*disque*TAILLEXUN;
			height = TAILLEYUN;
			dd.dessinerOvale(x, y, width, height);

		}
		if (this.disqueEnDeplacement != 0){

			int width = 2*disqueEnDeplacement*TAILLEXUN;
			dd.setCouleur(couleur(disqueEnDeplacement));
			dd.dessinerOvale(this.xDeplacement-width/2, this.yDeplacement-TAILLEYUN/2, width, TAILLEYUN);
		}
		this.affichagePasFini = false;

	}

	private Color couleur(int i) {
		return this.couleurs[i%3];
	}

	public void mouseDragged(MouseEvent e) {
		if (!this.resolution){
			int x,y;
			x = e.getX();
			y = e.getY();
			if (x<0)
				x = 0;
			if (y<0)
				y=0;
			if (x>LARG)
				x=LARG;
			if (y>HAUT)
				y=HAUT;
			if (disqueEnDeplacement == 0){			
				if ((x<LARG/3) && (x > 0)){
					if (piles[0].size() > 0){
						disqueEnDeplacement = piles[0].lastElement();
						this.origineDisque = 0;
						this.xDeplacement = x;
						this.yDeplacement = y;
						piles[0].remove(piles[0].size()-1);
						this.repaint();
					}
				}
				else if ((x<2*LARG/3) && (x > LARG/3)){
					if (piles[1].size() > 0){
						disqueEnDeplacement = piles[1].lastElement();
						this.origineDisque = 1;
						this.xDeplacement = x;
						this.yDeplacement = y;
						piles[1].remove(piles[1].size()-1);
						this.repaint();
					}
				}
				else if ((x<LARG) && (x > 2*LARG/3)){
					if (piles[2].size() > 0){
						disqueEnDeplacement = piles[2].lastElement();
						this.origineDisque = 2;
						this.xDeplacement = x;
						this.yDeplacement = y;
						piles[2].remove(piles[2].size()-1);
						this.repaint();
					}
				}
			}
			else{
				this.xDeplacement = x;
				this.yDeplacement = y;
				this.repaint();
			}
		}
	}

	public void verifier(){
		if ((piles[1].size() == this.nbreDisques) || (piles[2].size() == this.nbreDisques)){
			int min = (int) Math.pow(2, this.nbreDisques)-1;
			String appreciation = "Vous n'avez pas fait le nombre d'�tapes minimal qui est : "+min+"\nEncore un effort pour passer au niveau superieur ;-)";
			String titre = "Bravo !";
			if (this.nbreEtapes == min){
				this.nbreDisques++;
				titre = "Excellent !!!";
				appreciation= "Vous avez fait le nombre d'�tapes minimal :\nVous passez au niveau superieur !";
			}
			JOptionPane.showMessageDialog(this,"Vous avez gagn� en "+this.nbreEtapes+" �tapes ! "+appreciation,titre, JOptionPane.INFORMATION_MESSAGE);
			if (this.nbreDisques>20){
				JOptionPane.showMessageDialog(this,  "Vous avez GAGNE !!!","GAGNE",JOptionPane.INFORMATION_MESSAGE);
				this.nbreDisques = 2; 
			}
			this.repaint();
			this.reset();
			h.resoudre.setText("resolution automatique : non");
			h.resolution = false;
			this.resolution = false;
			this.th = new Etapes(piles,this);

		}
	}
	public void mouseMoved(MouseEvent e) {

	}


	public void mouseClicked(MouseEvent e) {


	}


	public void mouseEntered(MouseEvent e) {
		;

	}

	public void mouseExited(MouseEvent e) {


	}


	public void mousePressed(MouseEvent e) {
		;

	}


	public void mouseReleased(MouseEvent e) {
		if (!this.resolution){
			if (this.disqueEnDeplacement != 0){
				int x,y;
				x = e.getX();
				y = e.getY();
				if (x<0)
					x = 0;
				if (y<0)
					y=0;
				if (x>LARG)
					x=LARG;
				if (y>HAUT)
					y=HAUT;
				if ((x<LARG/3) && (x > 0)){
					if (piles[0].isEmpty() || (piles[0].lastElement() > disqueEnDeplacement)){					
						piles[0].add(disqueEnDeplacement);	
						if (this.origineDisque != 0){
							this.nbreEtapes++;
						}
					}
					else{
						piles[this.origineDisque].add(disqueEnDeplacement);
					}
					disqueEnDeplacement = 0;
				}
				else if ((x<2*LARG/3) && (x > LARG/3)){
					if (piles[1].isEmpty() || (piles[1].lastElement() > disqueEnDeplacement)){					
						piles[1].add(disqueEnDeplacement);	
						if (this.origineDisque != 1){
							this.nbreEtapes++;
						}
					}
					else{
						piles[this.origineDisque].add(disqueEnDeplacement);
					}
					disqueEnDeplacement = 0;
				}
				else if ((x<LARG) && (x > 2*LARG/3)){
					if (piles[2].isEmpty() || (piles[2].lastElement() > disqueEnDeplacement)){					
						piles[2].add(disqueEnDeplacement);	
						if (this.origineDisque != 2){
							this.nbreEtapes++;
						}
					}
					else{
						piles[this.origineDisque].add(disqueEnDeplacement);
					}
					disqueEnDeplacement = 0;
				}
				Hanoi.nbreEssais.setText("nombre d'essais : "+this.nbreEtapes);
			}			
			this.repaint();
			verifier();
		}
	}

	public boolean pasFini() {
		return affichagePasFini;
	}

}
