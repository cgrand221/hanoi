import java.util.Vector;


public class Etapes extends Thread{
	Vector<Integer>[] piles;
	Affichage a;
	private long temps;
	private int nbreEtapes;

	public void run() {		
		super.run();
		this.deplacer(a.nbreDisques, 0, 1);
		a.nbreEtapes = this.nbreEtapes;
		a.verifier();
	}
	public Etapes(Vector<Integer>[] piles,Affichage a) {
		this.piles = piles;
		this.a = a;
		this.temps = a.h.getTemps();
		this.nbreEtapes = 0;
	}
	public void setTemps(long t){
		this.temps = t;
	}

	void deplacer(int nbre,int source,int destination){
		if (nbre == 1){
			piles[destination].add(piles[source].lastElement());
			piles[source].remove(piles[source].size()-1);
			this.nbreEtapes++;
			Hanoi.nbreEssais.setText("nombre d'essais : "+this.nbreEtapes);
			a.repaint();
			dormir(a);
			return;
		}
		deplacer(nbre-1,source,non(source,destination));
		piles[destination].add(piles[source].lastElement());
		piles[source].remove(piles[source].size()-1);
		this.nbreEtapes++;
		Hanoi.nbreEssais.setText("nombre d'essais : "+this.nbreEtapes);
		a.repaint();
		dormir(a);
		deplacer(nbre-1,non(source,destination),destination);		
	}

	private void dormir(Affichage affichage) {
		while(affichage.pasFini());
		try {
			Thread.sleep(this.temps);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}
	private int non(int source, int destination) {
		return 3-source-destination;
	}

}


